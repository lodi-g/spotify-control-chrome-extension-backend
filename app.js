const axios = require('axios');
const bodyParser = require('body-parser');
const cheerio = require('cheerio');
const cors = require('cors');
const express = require('express');

const removeDiacritics = require('./removeDiacritics');
const settings = require('./settings');

const app = express();
const port = 8001;

const cache = [];

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => res.sendStatus(200));

app.post('/', async (req, res) => {
  let { artist, track } = req.body;

  if (!artist || !track) {
    return res.sendStatus(400);
  }

  artist = removeDiacritics(artist);
  track = removeDiacritics(track);

  const found = cache.find(song => song.artist === artist && song.track === track);
  if (found) {
    return res.send(found.lyrics);
  }

  const searchUrl = `${settings.api.host}/search?q=${artist} ${track}`;

  try {
    const searchRes = await axios.get(searchUrl, {
      headers: {
        Authorization: `Bearer ${settings.auth.access_token}`,
      },
      validateStatus: status => status < 500,
    });

    const { data } = searchRes;
    if (data.response.hits.length === 0) {
      return res.sendStatus(404);
    }

    const songUrl = data.response.hits[0].result.url;

    const songRes = await axios.get(songUrl, {
      validateStatus: status => status < 500,
    });

    const lyrics = cheerio
      .load(songRes.data)('.lyrics')
      .text();

    res.send(lyrics);

    cache.push({ artist, track, lyrics });
  } catch (e) {
    res.sendStatus(500);
  }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
